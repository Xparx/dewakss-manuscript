\section{Results}
\label{sec:results}

\subsection{Benchmarking the \dewakss algorithm}
\label{sec:algorithm}
\dewakss takes a normalized expression matrix and calculates a smoothed output expression matrix which has denoised gene expression for each cell. The \dewakss expression matrix will have decreased stochastic sampling noise; expression values, including zeros that are likely the result of undersampling, will be weighted according to their sample-to-sample context. We will test the effectiveness of diffusion and kNN-based (diffusion with step size = 1) denoising methods that are tuned with \dewakss objective function using four separate cases.

\subsection{\dewakss optimally groups cells and performs robustly, independent of data normalization method}
\label{sec:benchmark_tian2019}
Before any denoising can occur, single-cell data generally must be normalized. Recent work has established a benchmark for single-cell analysis methods which is broadly applicable to normalization and denoising techniques\cite{Tian2019}.
We have therefore compared the performance of \dewakss to several previously-described denoising methods using two benchmark datasets that are generated with different methods.
These artificially constructed RNA mixture datasets have a known ground truth; RNAmix\_CEL-seq2 is derived from the CEL-Seq2 method\cite{hashimshony2016} and RNAmix\_Sort-seq is derived from the SORT-seq method\cite{muraro2016}. Within each dataset, 'cells' that belong to the same group are samples that have the same proportions of mRNA from three different cell lines. Any differences between 'cells' in the same group can hence be attributed to technical noise.

Using a computational pipeline\cite{cellbench}, we test the effect of normalization methods on denoising techniques, with output scoring defined by a known ground truth (Figure \ref{fig:Tian2019_benchmark}).
Normalization methods are generally the same as previously tested\cite{Tian2019}, and include several bulk-RNA normalization methods (TMM, logCPM, DESeq2), several single-cell-specific methods (scone, Linnorm, scran), and a simple Freeman-Tukey transform (FTT).
Overall, we find that \dewakss yields expression profiles with high within-group correlation (averaged over all cells in the dataset) independent of the normalization method used, outperforming other denoising methods in the majority of cases (Figure \ref{fig:Tian2019_benchmark}A).
This is not due to high correlation between cells in different groups (which could indicate oversmoothing), as cells of the same type are strongly correlated and cells of different types are weakly correlated when plotted as a heatmap (Figure \ref{fig:heatmaps_normalization}).

\begin{figure*}[htb]
  \begin{center}
    \includegraphics[width=.9\textwidth]{figures/RNAmix_benchmark}
    \caption{\label{fig:Tian2019_benchmark} Benchmarking \dewakss on a predefined benchmark test\cite{Tian2019}. A) The average Pearson correlation coefficients between cells that are known to be in the same group, calculated as in \citet{Tian2019} for the RNAmix\_CEL-seq2 and RNAmix\_Sort-seq benchmark datasets. \dewakss yields highly correlated expression profiles for 'cells' in the same group, robustly across different normalization methods. B) Self-supervised hyperparameter grid search results (RNAmix\_Sort-seq normalized by FTT). Neighbors are on the x-axis and PCs are colored. The optimal configuration neighbors are shown by the dotted black line and PCs are shown by the solid black line. C) Optimization behavior using optimal PCs=4 found in (B) for 5-200 neighbors. The lowest prediction error for each diffusion trajectory (line) is marked by a circle with a green outline if it corresponds to the number of iterations in the optimal configuration \(i=1\) or in black when the optimal number of iterations \(>1\). The optimal value is marked by a diamond. The number of diffusion steps decreases as the number of neighbors increases. The number of diffusion steps is truncated to 9 steps. The prediction error decreases as the number of neighbours increases from 5-80, and then increases.}
  \end{center}
\end{figure*}

\dewakss has three essential input hyperparameters: the number of principal components (PCs) for initial data compression, the number of nearest neighbors to build the graph embedding with (\(k\)), and the connection mode for edge weights (either normalized distances or network density weighted connectivities).
For this benchmark, the \dewakss algorithm has grid searched through a model hyperparameter space, testing connection mode \{\mbox{distances, connectivities}\}, number of neighbors \{\mbox{1, 2, ..., 20, 30, 40, ..., 150, 200}\}, and PCs \{\mbox{1,2, ..., 20, 30, 40, ..., 150, 200}\}.
Self-supervision selects optimal model hyperparameters by minimization of mean squared error (MSE); for the RNAmix\_Sort-seq dataset that has been normalized by FTT, this is the normalized distance mode with 4 PCs and 80 neighbors (Figure \ref{fig:Tian2019_benchmark}B).
The optimal selection of hyperparameters varies from dataset to dataset and by normalization method (Table \ref{tab:hp_search_results}).
In most cases, the optimal MSE is found for the parameters given by normalized distances with between 50-130 neighbors and 3-13 PCs, but data which has not been normalized has very different optimal model hyperparameters. In general, using normalized distances as edge weights between neighbors outperforms using connectivities. In all cases, the optimal number of diffusion iterations is 1 (no diffusion) given a specific set of PCs and \(k\), indicating that diffusion is not optimal on this data. The small number of PCs and large value for optimal neighbors suggests that this dataset is simplistic with weak local structure, which is a reasonable expectation given the artificial construction of the RNAmix datasets. The MSE is scaled differently depending on the data normalization and should not be compared between methods.
%%TODO: Fig 2C results

\begin{table*}[t]
  \centering
  \begin{tabular}{lrrlrrlrl}
    \hline
    Normalization  &  Dataset  &   iteration &mode           &   neighbors &   pcs &   MSE  \\
    \hline
    DESeq2         &  celseq2  &           1 &distances      &         120 &     3 & 0.466  \\
    FTT            &  celseq2  &           1 &distances      &          90 &     5 & 0.878  \\
    Linnorm        &  celseq2  &           1 &distances      &         110 &     4 & 0.066  \\
    logCPM         &  celseq2  &           1 &distances      &         100 &     6 & 4.567  \\
    none           &  celseq2  &           1 &connectivities &          14 &   120 & 4.428  \\
    scone          &  celseq2  &           1 &distances      &         120 &     4 & 0.445  \\
    scran          &  celseq2  &           1 &distances      &         130 &     3 & 0.484  \\
    TMM            &  celseq2  &           1 &distances      &          50 &     6 & 0.378  \\
    \hline
    DESeq2         &  sortseq  &           1 &distances      &         100 &     3 & 0.513  \\
    FTT            &  sortseq  &           1 &distances      &          80 &     4 & 1.127  \\
    Linnorm        &  sortseq  &           1 &distances      &         100 &     4 & 0.083  \\
    logCPM         &  sortseq  &           1 &distances      &          80 &    13 & 4.684  \\
    none           &  sortseq  &           1 &distances      &          10 &    17 & 5.321  \\
    scone          &  sortseq  &           1 &distances      &         100 &     4 & 0.484  \\
    scran          &  sortseq  &           1 &distances      &         120 &     3 & 0.536  \\
    TMM            &  sortseq  &           1 &distances      &          50 &     6 & 0.412  \\
    \hline
  \end{tabular}
  \caption{Optimal hyperparameters selected by \dewakss self-supervised objective function}
  \label{tab:hp_search_results}
\end{table*}

\subsection{\dewakss maintains cluster homogeneity and deconvolves cluster structure comparably to state-of-the-art methods.}
\label{sec:celltype_benchmark}

To evaluate \dewakss on higher-complexity data, we adapt the benchmark used by DeepImpute\cite{Arisdakessian2019}, using a dataset which contains 33 annotated cell types in primary visual cortex cells from mice (GSE102827)\cite{Hrvatin2018}.
We evaluate performance after preprocessing and denoising the count data by defining experimental clusters using the Leiden algorithm\cite{Traag2018leiden}, which tunes the number of clusters based on merging and partitioning the kNN-G to find an optimal partitioning given the resolution parameter \(r\).
Differences between annotated cell types and experimental clusters are quantified by the Fowlkes-Mallows score.
The silhouette score estimates the relative within-cluster density of the annotated cell-type clusters compared to the closest annotated neighbor-cluster distances -- and is, therefore, independent of the cluster algorithm choice -- and evaluates the closeness of known cell identities through different data transformations. The silhouette score is separately calculated on two dimension-reduction projections, (i) using 2 UMAP\cite{UMAP} components, and (ii) using PCA with the number of components used to compute the kNN-G used in the Leiden and UMAP algorithms.

We compare the results of DeepImpute, \dewakss, MAGIC, DrImpute and SAVER to count data that has been preprocessed but has otherwise not been denoised (pp). The preprocessing is detailed in section \ref{sec:preprocessing}, and, in short, consists of filtering and median normalizing the data followed by a Freeman-Tukey transformation.
DeepImpute\cite{Arisdakessian2019} takes as input the raw count data and needs to be preprocessed after, as above. MAGIC is run using the Seurat pipeline\cite{Stuart2018}.
For this dataset, \dewakss selects 100 PCs and 150 neighbors as optimal hyperparameters (Figure \ref{fig:Arisdakessian2019_benchmark}A)).
After denoising we evaluate the performance metrics with a range of clustering and dimensionality reduction parameters to estimate the sensitivity of the performance metrics (clustering and cell dispersion during projection) to the choices of these parameters (Figure \ref{fig:Arisdakessian2019_hyperparams}).
Overall performance (as determined by MSE) is poor when using few components (PCs) and a small number \(k\) of neighbors, which is similar to the default parameters in many processing pipelines (Figure \ref{fig:fm_benchmark_scores}).
This underlines the importance of carefully considering the amount of variance to be used in the initial kNN-G construction.

Because the number of inferred clusters influences the Fowlkes-Mallows score, we also adjust, by applying a factor, doubling, quadrupling or halving, the resolution parameter \(r\)  of the Leiden clustering algorithm to increase or decrease the number of clusters to be closer to the number of annotated clusters (33). To be able to run DrImpute and SAVER we down-sample the dataset to \(10\%\) of the annotated cells including all 33 clusters before denoising using 4800 cells.
\(r\) is increased from 1 to 2 for DeepImpute and pp, increased from 1 to 4 for DrImpute and SAVER, and decreased from 1 to 0.5 for MAGIC. \dewakss is not adjusted as the number of clusters falls close to the number of annotated clusters by default.

\begin{figure*}[htb]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{figures/Arisdakessian2019_benchmark}
    \caption{\label{fig:Arisdakessian2019_benchmark} Denoising celltype-annotated data from \citet{Hrvatin2018} using metrics from \citet{Arisdakessian2019}. The dataset contains 33 annotated celltypes in 48267 cells. A) Optimal denoising of the expression data with \dewakss requires 100 PCs and \(k=150\) neighbors. B) We benchmark six different denoising pipelines: (i) in-house preprocessed (section \ref{sec:preprocessing}), (pp), (ii) DeepImpute, (iii) \dewakss, (iv) MAGIC, (v) DrImpute and (vi) SAVER. To be able to run (v) and (vi) we down-sample the data to \(10\%\) of the annotated cells.  After preprocessing \& denoising, data is clustered with the Leiden algorithm\cite{Traag2018leiden} using 300 PCs and 150 neighbors (resolution is set to \(r=1\) for \dewakss, \(r=2\) for preprocessed (pp) and DeepImpute, \(r=4\) for DrImpute and SAVER, and \(r=0.5\) for MAGIC). Algorithm performance is measured with the Fowlkes-Mallows metric and silhouette score on two representations of the data, PCA and UMAP}
  \end{center}
\end{figure*}

\subsection{Optimal kNN denoising does not involve diffusion}
\label{sec:no_diffision}

On all test datasets, we observed that the optimal configuration was found to have a single iteration (no diffusion) but variable (dataset specific) optimal number of PCs and neighbors (Figure \ref{fig:Tian2019_benchmark}C).
This observation extended to all normalization methods if parameter spaces with sufficient numbers of neighbors were explored (Table \ref{tab:hp_search_results}).
To determine if diffusion is improving denoising for real-world data, we applied \dewakss to seven published single-cell datasets.
We tested on mouse bone marrow (BM) data\cite{Paul2015}, on human cell line epithelial-to-mesenchymal transition (EMT) data\cite{MAGIC}, on \Yeast data from rich media (YPD) and on \Yeast data after treatment with rapamycin (RAPA)\cite{Jackson2020}, on mouse visual cortex tissue (VisualCortex) data\cite{Hrvatin2018}, on human embryonic forebrain tissue (hgForebrainGlut) data and on mouse dentate gyrus granule neuron (DentateGyrus) data\cite{LaManno2018}.
The BM and EMT datasets are preprocessed following the vignette provided by the MAGIC package\cite{MAGIC} (section \ref{sec:preprocessing_magic}). The YPD, RAPA and VisualCortex datasets are preprocessed using the procedure in section \ref{sec:preprocessing}. The hgForebrainGlut and DentateGyrus datasets are preprocessed with the velocyto\cite{LaManno2018} and SCANPY\cite{SCANPY} python packages using the provided vignettes (section \ref{sec:preprocessing_velocyto}).

We run \dewakss on these datasets (searching for hyperparameters using \(\sim\)equidistant values in log space) to find the optimal configuration (Figure \ref{fig:opt_parameter_trends}).
For the BM dataset we let the algorithm run 20 diffusion steps to map out the objective function.
For all other datasets we use \emph{run2best}, which finds the first minimum MSE during diffusion and then stops the search (figure \ref{fig:opt_parameters_search}).
All six real-world datasets result in optimal MSE when there is no diffusion (number of iterations \(i=1\)) (Table \ref{tab:hp_search_results_all_data}).

\begin{table*}[t]
  \centering
  \begin{tabular}{llrrlrrl}
    \hline
    Dataset                            &    iteration &   MSE & mode           &   neighbors &   PCs \\
    \hline
    BM \cite{Paul2015}                 &            1 & 0.311 & distances      &         100 &    50 \\
    EMT \cite{MAGIC}                   &            1 & 0.222 & distances      &         100 &   100 \\
    % \citet{Zeisel2015}                 &            1 & 0.591 & connectivities &         200 &   100 \\
    VisualCortex \cite{Hrvatin2018}                &            1 & 0.132 & distances      &         150 &   100 \\
    YPD \cite{Jackson2020}     &            1 & 0.217 & distances      &         150 &    50 \\
    RAPA \cite{Jackson2020} &            1 & 0.261 & distances      &         175 &    20 \\
    hgForebrainGlut \cite{LaManno2018} &            1 & 0.106 & distances      &         100 &    20 \\
    DentateGyrus \cite{LaManno2018}    &            1 & 0.055 & distances      &         100 &   100 \\
    \hline
  \end{tabular}
  \caption{Optimal configurations found by hyperparameter search on \dewakss on seven real-world single-cell datasets}
  \label{tab:hp_search_results_all_data}
\end{table*}

\subsection{\dewakss preserves data variance for downstream analysis}
\label{sec:magic_comparison}

The main goal of denoising scRNA-seq data is to reduce the influence of noise and to reveal biological variance.
High dimensional biological data contains some variance which is due to random noise and should be removed, and some variance that is due to biological differences and should be retained.
Removing noise is important for correct interpretation of patterns in the data, but attenuating biological variation eliminates biological signal and can result in biased analyses.
Biological variance is not easily separable from technical noise, and denoising methods risk oversmoothing, retaining only the strongest patterns (\eg the first few principal components of the data) while discarding informative minor variation.
It is therefore critical when tuning model parameters to have an objective function that takes into account the total variance of the data structure.

We evaluate the effect that denoising has on data variance by comparing the singular value structure of the denoised data for different methods, which represents the relative variance of all dimensions.
Although the optimal amount of variance and the number of components that capture that variance is not known, we reason that comparing the relative variance in all dimensions allow us to determine the extent to which a denoising method is smoothing the data.
Denoised data that requires fewer components to capture most (>90\%) variance is more smoothed. 
When most variance is compressed into a handful of principal components, features within the data become collinear, more complex interactions between features disappear, and only the strongest sources of variance are preserved.
Some downstream analyses are likely to be more sensitive to this oversmoothing than other analyses. 
For example, a clustering approach may still effectively separate groups based on variance in only a few dimensions, but regulatory inference may be substantially confounded or uninterpretable.

MAGIC\cite{MAGIC} is currently among the most popular algorithms for denoising single-cell RNA-seq data.
It uses a heuristic for determining optimal smoothing; as published, it used \(\Delta R^{2}\) between diffusion steps, but the most recent implementation has switched to Procrustes analysis of the differences between diffusion steps.
Neither approach has an objective way to determine optimal smoothing.
In the absence of crossvalidation or some other external method that prevents overfitting, we expect \(R^{2}\) to decrease until all data is averaged, \ie, to saturation, and a Procrustes analysis should behave similarly.
MAGIC addresses this by hard-coding a stopping threshold which determines when the data is smoothed ``enough''; because this threshold is not data-dependent, it can result in highly distorted outputs\cite{Jackson2020, Arisdakessian2019, Chen2018}.

If this threshold is converted to a model hyperparameter, it is still necessary to tune with some external method as it has no lower bound for arbitrarily poor estimates.

We compare the effects of denoising using a heuristic as implemented in MAGIC\cite{MAGIC}, using \dewakss in its optimal configuration and using \dewakss in an oversmoothing (non-optimal) configuration for comparison. We also run this comparison for DeepImpute\cite{Arisdakessian2019}, DrImpute\cite{Gong2018} and SAVER\cite{Huang2018} with default configurations.
This comparison is performed on the previously-described mouse BM, preprocessed using the approach described in \cite{MAGIC}.
We run MAGIC with three sets of model parameters; the default parameters, default with early stopping (the diffusion parameter \(t=1\)), and with the decay parameter \(d=30\).
For \dewakss, we scan a log-equidistant parameter range for the optimal configuration (Figures \ref{fig:paul2015_hp_search} and \ref{fig:paul2015_exaustive_search_distances}) and find that the optimal configuration uses normalized distances with number of neighbors \(k=100\) and with number of principal components PCs\(= 50\) for \(i=1\), giving  \(\MSE=0.3107\).
Diffusion iterations \(i\) increment until a minimum MSE is found.
For the BM data, the MSE generally decreases as the number of PCs increases.
Beyond a certain point, however, continuing to increase the number of PCs (to 500) increases the MSE.
The optimal number of PCs, \(50\), is small compared to the size of the data and suggests that some compression of the data is optimal before running the kNN algorithm.
To oversmooth the data we extended the number of iterations to run \dewakss to \(i=4\), beyond the optimal number of iterations  (Figure \ref{fig:paul2015_denosing}B).
We also used the molecular cross-validation (MCV) application of noise2self\cite{Batson2019MCV}, implemented as in Section \ref{sec:pca}, to select the optimal number of PCs for denoising by \dewakss. 
We found that MCV selected fewer PCs for denoising compared to the \dewakss objective function (Figure \ref{fig:mcv_paul215}; 13 PCs compared to 50 PCs).

To investigate how the variance structure of the data changes based on denoising we compute the singular values (section \ref{sec:var-calc}) and determine the number of components needed to explain \(90\%\) and \(99\%\) of the variance for each dataset after denoising (Figures \ref{fig:paul2015_denosing}A and \ref{fig:paul2015_explained_variance}).
We observe a striking difference between the oversmoothed data and the optimally denoised data.
With optimal denoising, \(90\%\) of the variance is captured by \(259\) components.
Utilizing the MCV method to select hyperparameters resulted in an intermediate amount of variance retained after denoising when compared to the \dewakss optimal and the \dewakss oversmoothed denoised data.
Only \(2\) components are needed to capture \(90\%\) of the post-processing variance when oversmoothing the data, showing that a substantial portion of the original information content of the data is lost in this regime.
\dewakss oversmoothing is comparable to the results of using MAGIC with default parameters, where \(90\%\) of the variance in the post processed data can be captured with only \(3\) components.
When using only one iterative step and default parameters, MAGIC captures this amount of variance using \(25\) components.
In most cases the MAGIC algorithm generates shallow variance structures with a few components needed to express nearly all of the variance.
The variance structure can differ greatly depending on the hyperparameters chosen for \dewakss, and poor parameter selection results in shallow variance structures.
However, the objective function automatically identifies an optimal configuration such that we expect to keep the relevant variance. For SAVER the first component capture almost all the variance while the 21 subsequent components capture the remaining variance implying that SAVER compress the data significantly. For DeepImpute and DrImpute the opposite is the case that the \(99\%\) of the variance are captured by over \(80\%\) of the components implying that these methods maintains a more flat distribution of the data over the components.

We can see the consequence of oversmoothing when plotting the expression of the erythroid marker Klf1, the myeloid marker Mpo, and the stem cell marker Ifitm1 (Figure \ref{fig:paul2015_denosing}C).
Very few individual cells express both Klf1 and Mpo in the optimally-denoised data, but the oversmoothed data implies that there is a smooth continuous transition from high Klf1 expression, through co-expression of Klf1, Mpo, and Ifitm1 markers, to high Mpo expression.
Although the difference in MSE is not large (\(\Delta\) MSE \(< 0.0175\)) between these two denoised datasets, the resulting biological interpretation differs a great deal, and likely highlights a spurious relationship in the oversmoothed case.

\begin{figure*}[htb]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{figures/magic_Paul2015_comparison}
    \caption{\label{fig:paul2015_denosing} Mouse bone marrow (BM) data denoising. A) The numbers of principal components needed to explain \(99\%\) and \(90\%\) of the variance in the data for different hyperparameter values for \dewakss and MAGIC. \dewakss is run with \textbf{optimal} parameters (\(k=100\), \(PCs=50\), \(i=i_{\min MSE}\)), with \textbf{oversmoothed} parameters (\(k=100\), \(PCs=50\), \(i=i_{\min MSE}\)), with \textbf{robust} parameters (\(k=10\), \(PCs=13\) selected using MCV as in section \ref{sec:pca}, \(i=i_{\min MSE}\)), and as \textbf{X base}, where normalized expression values are used instead of PCs with (\(k=100\), \(i=i_{\min MSE}\)). MAGIC is run with \textbf{defaults} (\(d=15\), \(PCs=100\), \(k=15\)), with early stopping \textbf{t1} (\(t=1\)), and with \textbf{d30} (\(d=30\)). B) Expression of erythroid marker gene Klf1, myeloid marker Mpo, and stem cell marker Ifitm1 in \dewakss optimal and \dewakss oversmoothed data. The MSE increases in each iteration. C) The objective function output as a function of diffusion steps for the optimal number of PCs \(=50\). The minimum MSE is found for 100 neighbors and 1 diffusion step, \ie, using only the selected 100 neighbors.}
  \end{center}
\end{figure*}

We run a similar analysis on the EMT data comparing \dewakss and MAGIC (Figure \ref{fig:emt_denosing}) and find identical effects.

\subsection{\dewakss improves recovery of differentially expressed genes}
\label{sec:degs}
Biological analysis of gene expression data often requires determining differentially expressed genes (DEGs) between groups of cells.
We evaluate the effect of denoising on discovery of DEGs by comparing DEGs from a subset of cells with genetic perturbations\cite{Jackson2020} to the equivalent bulk microarray data\cite{Kemmeren2014}.
Eleven different gene deletion strains are compared to a wild-type control. 
Five of these gene deletion strains have \emph{few} DEGs versus six strains with \emph{lots} of DEGs; gene deletion strains with more than 63 DEGs (1\% of genes in the yeast genome) in the bulk data are considered to have lots of DEGs.  
DEGs from the single-cell data are determined by wilcoxon rank sum test with Benjamini/Hochberg correction \(\alpha=0.01\).

Most methods increase DEG recovery from single-cell data compared to preprocessing alone (Figure \ref{fig:lots_deg_gs}), with SAVER performing the best and \dewakss in second.
However, performance on test subsets with few DEGs is generally low for all methods (Figure \ref{fig:few_deg_gs}), although these results may be less reliable due to the large effect of single DEGs on performance metrics. 


\begin{figure*}[htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{figures/DEG_lots_yeast_analysis_v2}
    \caption{\label{fig:lots_deg_gs} Differentially expressed genes (DEGs) between bulk and single cell data. Top panel is Jaccard index between bulk DEGs and single cell DEGs at FDR \(=0.01\) for each separate deletion strain. Bottom panel is AUROC for single-cell DEGs ordered by adjusted p-value. \emph{preprocessed} is count normalized and log-transformed with no denoising method.}
  \end{center}
\end{figure*}


\subsection{\dewakss scales to large single-cell data sets}
Single-cell data sets are continuing to grow in scale, and therefore denoising algorithm performance is an important consideration. 
We have benchmarked several denoising methods on a standard laptop (details in \ref{sec:speedbenchmark}) in order to evaluate speed and scalability.
The size of the datasets measured are detailed in table \ref{tab:performance_times}.
We find that in our desktop-scale test, \dewakss is able to analyze the largest data set (64.8k cells x 18.1k genes) in a reasonable time (Figure \ref{fig:performance_times}), although DeepImpute and MAGIC are faster.
Other methods are not able to run to completion in the larger data sets with the computational resources provided for this test.

\begin{figure*}[htb]
  \begin{center}
    \includegraphics[width=0.75\textwidth]{figures/Performance_times_benchmark_figure}
    \caption{\label{fig:performance_times} Computational performance of all tested method on selected datasets. Runtime (minutes) is plotted against the total number of values (cells * genes) in the dataset, to account for differing numbers of genes in each data set. Complete results table is available in Table \ref{tab:performance_times}.}
  \end{center}
\end{figure*}
