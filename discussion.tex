\section{Discussion}

In this paper we have introduced a novel objective function, based on noise2self\cite{Batson2019Noise2SelfBDPP}, and applied it to self-supervised parameter tuning  of weighted k-nearest neighbors (kNN) and diffusion-based denoising.
The resulting algorithm, \dewakss, is specifically designed to denoise single-cell expression data.
The objective function has a global objective that can be minimized, removing the need to use often unreliable heuristics to select model parameters, which is a drawback to many current single-cell denoising methods.
We demonstrate that this framework accurately denoises data by benchmarking against previously established methods, and find that it is robust to choice of normalisation method (Section \ref{sec:benchmark_tian2019}).

Due to the difficulty in establishing a ground truth for most real-world single-cell data, denoising algorithms are frequently tested on synthetic or artificial data.
Maintaining biological variance is a crucial aspect of denoising; common downstream applications such as cell type identification, differential gene expression, marker identification, and regulatory network inference rely on biological variance to function.
We therefore believe that it is necessary to extensively test on experimental data (a notable strength of \citet{MAGIC} is testing on real-world data). 
On larger datasets with higher complexity, \dewakss performs well in terms of deconvolving cell types.
We find that in general, the amount of variance included when clustering the data has a large impact on the performance of all methods tested, and that \dewakss outperforms other denoising algorithms in this area.
While it is still an open question how much variance should be used to project and cluster single-cell data, it is clear that it is an essential component of accurate interpretation.

To investigate the properties of our method we run the algorithm on seven different published single-cell gene expression datasets. 
In all cases, the optimal denoising configuration (as determined by the objective function) uses the closest neighborhood, and is not improved by diffusion on the kNN graph. 
Diffusion causes a decrease in denoising performance, compressing almost all of the variance into a handful of dimensions.
This may have some advantages for visualizing high-dimensional gene expression data, but most non-visualization analyses are impaired by the loss of variance.
We also find that the number of neighbors \(k\) and the number of principal components to use tend to be large compared to the default parameters of other methods and conventions used in computational pipelines.
In general, there is an advantage to the inclusion of more principal components than called for by common rules of thumb, like using the knee in an explained variance vs number of components plot.
However, including an arbitrary number of principal components is not ideal, as excess principal components do decrease performance.
Comparing the use of a distance matrix versus the use of a connectivity matrix as a representation of the kNN-G shows that a distance matrix yields better results. 
The degree of similarity between the expression profile of one cell to that of another cell is relevant for denoising, not just whether cells are more or less similar than other cells' expression profiles in the experiment. 

Overall, the \dewakss framework presented here has substantial advantages over heuristic parameter selection. 
Heuristic-based denoising methods set hyperparameters without a clear basis for effectiveness, often with opaque reasoning for choices.
At best, this is likely to result in sub-optimal denoising performance; at worst, it may result in data that is dominated by oversmoothing effects, and which yields incorrect biological interpretations. 
Our objective function-based method provides a rigorous way of choosing an effective configuration.
The difficulties of evaluating how to denoise single-cell data should not be underestimated.
It is vital that the effectiveness of single-cell processing methods be quantifiable, so that the methods can be tuned for performance. 
We have chosen to use Euclidean distances for all analysis, but \dewakss can accept any graph derived with any distance metric to create the kNN matrix.
By constructing a denoising function that uses a \(k\)-nearest neighbors graph and is consistent with the conditions laid out in noise2self, we have derived an easily-evaluated method that can denoise single-cell data in a self-supervised manner.
The \dewakss objective may also have applications to other graph-based algorithms.
