\section{Methods}
\label{sec:methods}
We begin by presenting a review of the mathematical constraints on our denoising function. We then present
the core \dewakss method and objective function. We end with descriptions of our preparatory and preprocessing methods.

\subsection{Fundamental principle of noise2self}
\label{sec:noise2self}

\citet{Batson2019Noise2SelfBDPP} present the approach noise2self and applied it for UMI counts as the method Molecular Cross Validation (MCV)\cite{Batson2019MCV}, in which they partition observed features (in this case raw transcript counts) \(x_{i \in \mathbb{J}}\), \(\mathbb{J} = \{1,\ldots,2m\}\), into two groups \(\{X_J, X_{J^{c}}\} = \{\{x_1,\ldots, x_m\},\{x_{m+1},\ldots, x_{2m}\}\} \) where the superscript \(c\) represents the complement set. 
The task is then to find an invariant function \(g(x)_{J}: \mathbb{R}^{2m} \rightarrow \mathbb{R}^{2m}\) that operates only on \(X_{J^c}\) and yields an output \(\hat{x}\) whose entries at indices \(J\) are predictors of \(x_J\). 
This function is independent of \(x_J\); some of the features of each datapoint are predicted using another independent set of features of the same datapoint.
MCV was implemented for reference (section \ref{sec:pca}) and for selecting a `robust` set of PCs in section \ref{sec:magic_comparison}. MCV applied for PCA component selection optimally selects linearly separable components that are informative and the data is constrained to that sub-selection. Figure \ref{fig:mcv_paul215} shows the recreated analysis done by \citet{Batson2019Noise2SelfBDPP}. This practical implementation of the original noise2self principle does not employ a kNN graph and is linear as oppose to the non-linearity of the graph appraoch.

\subsection{Denoising expression data with a weighted affinity kernel and self-supervision}
\label{sec:dewakss-markov}

In \dewakss, as in other state-of-the-art methods, we start by computing a lower, \(d\)-dimensional representation of the data using PCA. We then compute a connectivity or distance-weighted kNN-G with \(u\) neighbors. %% cite seurat and others
Our approach is similar to that of MAGIC\cite{MAGIC} but differs in two key ways: (i) we use a self-supervised objective function for hyperparameter selection, and (ii) we denoise on the expression values directly to avoid a loss of information/variance that results from overreduction of dimensionality (reducing the data to a latent representation with low rank or low dimensionality such that key biological variation is lost).
To calculate the kNN-G, we use the algorithm UMAP\cite{UMAP} and its implementation\cite{UMAPsoftware} and create a right-stochastic matrix \(\mM_{d, u}\) from the connectivity/distance matrix.
In practice, any graph can be provided as input to \dewakss for denoising. 
We use the UMAP neighbor algorithm due to its versatility and speed, but alternative methods could be used here. The UMAP implementation only uses the neighbour search algorithm if the number of cells is above 4096 by default and otherwise computes all distances and picks the k closest ones.

Denoising using a normalized kNN-G \(\mM\) can be carried out through simple matrix multiplication
\begin{equation}\label{eq:non-markov}
  \begin{aligned}
    \check{\mX} &= \mM \mX\\
    \check{\mX}_{2} &= \mM \check{\mX}\\
  \end{aligned}
\end{equation}
and so on, where \(\mX\) is the expression matrix for \(z\) cells, with each column \(\bx_{*j}\) containing the expression values of a single gene \(j\) for each cell \(k \in K = \{1, \ldots, z\}\), \(\check{\mX}\) is \(\mX\) after one step of denoising and \(\check{\mX}_{2}\) is \(\mX\) after two diffusion steps of denoising. For a given gene \(j\) in a single cell \(k\), this equation calculates the weighted average influence of the expression value of the same gene in each neighboring cell. The expression value \(\check{x}_{kj}\) is hence set to this weighted sum:

\begin{equation}
    \check{x}_{kj} = \sum_{\hat{k}=1}^z \mu_{k\hat{k}} x_{\hat{k}j}
    \label{eq:matrix-entry}
\end{equation}
where \(\mu_{k\hat{k}}\) is the \(k\hat{k}\)-th element of \(\mM\) and \(\sum_{\hat{k}=1}^z \mu_{k\hat{k}} = 1\).

In general, a Markov process can be forward-iterated as
\begin{equation}\label{eq:2step-markov}
  \mM^{2}= \mM\mM
\end{equation}
for a two-step iteration, generalized to \(\mM^{n}\) for an \(n\)-step forward process. Denoising is then carried out as follows:
\begin{equation}\label{eq:nstep-markov}
  \check{\mX}_{n} = \mM^{n} \mX
\end{equation}

In \dewakss we implement a self-supervised procedure by noting that the operation in equation \ref{eq:nstep-markov} is the application of an invariant function \(g(x)_{J}\) on each entry of \(\mX\) if the diagonal elements of \(\mM\) at each step \(n\) are set to 0 (to enforce the independence noted in section \ref{sec:noise2self}). If diagonal elements are not forced to zero the second step of the diffusion becomes self referential and enforces overfitting of the objective function (Figure \ref{fig:dewakss_self_ref_paul2015}). 
Here \(J = \{j\}\), so the expression value of a gene \(j\) in cell \(k\) is calculated using the expression values of \(j\) in all cells except cell \(k\). Equation \ref{eq:matrix-entry} reduces to:
\begin{equation}
    \check{x}_{kj} = \sum_{\hat{k} \neq k}^z \mu_{k\hat{k}} x_{\hat{k}j}.
    \label{eq:matrix-entry-2}
\end{equation}
That is, for each gene there is an invariant function over the expression values of the gene across all cells.

The Markov property guarantees the independence of the neighborhood graph from past steps.
At each step, we set \(\diag{\mM} = 0\) and renormalize to a right stochastic matrix.
Let \(s\) be a function that removes the diagonal elements of a matrix and then normalizes the resulting matrix to a right stochastic matrix. Let \(\mu^{(d, u, n)}_{k\hat{k}}\) be the \(k\hat{k}\)-th element of \(\mM_{d, u}^{\bar{n}}\). Then
\begin{equation}
  s(\mM_{d, u}^{\bar{n}}) = \mL_{d, u, n}^{-1} (\mM_{d, u}^{\bar{n}} - \mV_{d, u, n})
\end{equation}
for each \(n\) with
\begin{equation}
  \mV_{d, u, n} =
  \begin{bmatrix}
    \mu^{(d, u, n)}_{1,1} & & \\
    & \ddots & \\
    & & \mu^{(d, u, n)}_{z,z}
  \end{bmatrix}
\end{equation}
and
\begin{equation}
  \mL_{d, u, n} =
  \begin{bmatrix}
    \sum_{j\neq 1} \mu^{(d, u, n)}_{1,j} & & \\
    & \ddots & \\
    & & \sum_{j\neq z} \mu^{(d, u, n)}_{z,j}
  \end{bmatrix}
\end{equation}

with the following notation:
\begin{equation}\label{eq:2step-s-markov}
  \begin{aligned}
    \mM_{d, u}^{\bar{2}} &= s(\mM_{d, u}) s(\mM_{d, u})\\
    \mM_{d, u}^{\bar{3}} &= s(\mM_{d, u}^{\bar{2}}) s(\mM_{d, u}) . \\
  \end{aligned}
\end{equation}
Equation \ref{eq:nstep-markov} can be rewritten and incorporated into the mean square error minimization objective as follows:
\begin{align}\label{eq:optimize-dewakss}
  d^*, u^*, n^* &= \argmin_{d, u, n} \norm{s(\mM_{d, u}^{\bar{n}})\mX - \mX} \\
  \mX^* &= s(\mM_{d^*, u^*}^{\bar{n^*}})\mX
\end{align}
We use this equation to find \(n\) that denoises \(\mX^*\) enough to best capture its underlying structure while attenuating variation due to noise. To make sure we keep the invariant sets independent we consider each step an independent Markov process and apply the function \(s(.)\) at each step.

\subsection{Converting kNN to a right stochastic transition matrix}
To allow for the use of UMAP distance metrics, \dewakss uses the transformation on distances used in \cite{MAGIC},
\begin{equation}
\mM = e^{-(\mD/\bar{\md}).^{\alpha}}
\end{equation}
where \(\mD\) is the matrix of distances between the gene expression profiles of different cells and \(\bar{\md}\) is the mean of the nonzero elements of \(\mD\). A decay rate \(\alpha\) is also used, and the \(.\) in the equation indicates element-wise multiplication.
This decay rate is applied on a connectivity matrix as \(\mC^{.\alpha}\), where \(\mC\) has elements \(c \in [0,1]\). It should be noted that in \cite{MAGIC}, the decay rate is also applied during the construction of the kNN-G to estimate distance thresholds and there may not be a 1-to-1 correspondence in the algorithms. To stabilize the denoising procedure, the final step before normalizing to a right stochastic transition matrix is to symmetrize \(\mM\) so that
\[\mM = \frac{\mM + \mM'}{2}\]

\subsection{Preprocessing of scRNA-Seq expression data}
\label{sec:preprocessing}

We preprocess single cell RNA-seq datasets before applying denoising.
Unless otherwise stated, all datasets are preprocessed using the same steps if no guidelines were provided from the dataset or publication source documents.

Preprocessing is carried out using the following steps: (i) Filtering cells by using only those that have greater than a certain number of expressed genes and greater than a certain total cell UMI count. This is done mainly by visual inspection, clipping \(1-2\%\) of the data -- in practice, removing the top and bottom \(0.5\) percentile of data points; (ii) Removing genes not expressed in more than \(n\) cells, with \(n\leq 10\); (iii) Normalizing the counts of each cell so that each cell has the same count value as the median cell count over the cell population; and (iv) applying the Freeman-Tukey transform (FTT)\cite{freeman1950} with an adjustment term \(-1\) to preserve sparsity,

\begin{equation}
  \Tilde{\mbox{FTT}}(x) = \sqrt{x} + \sqrt{x+1} - 1
\end{equation}
The FTT stabilizes the variance of Poisson distributed data. \citet{Wagner2017} showed that the FTT is a good choice for single-cell data compared to the log-TPM and log-FPKM transforms as it does not underestimate the relative variance of highly expressed genes and thus balances the influence of lowly expressed gene variation. In other words, the relative variance of highly expressed genes versus more lowly expressed genes should be preserved after transformation. This is an essential property for inferring a relevant kNN-G.

We process all data with the help of the SCANPY framework\cite{SCANPY}. \dewakss can accept the SCANPY AnnData object, a regular numpy array or a scipy sparse array as input.

\subsection{Preprocessing for comparison with MAGIC}
\label{sec:preprocessing_magic}
The BM dataset is preprocessed using the same approach as used by \cite{MAGIC},
as detailed here: \url{https://nbviewer.jupyter.org/github/KrishnaswamyLab/MAGIC/blob/master/python/tutorial\_notebooks/bonemarrow\_tutorial.ipynb})
The EMT dataset is preprocessed as detailed here: \url{https://nbviewer.jupyter.org/github/KrishnaswamyLab/magic/blob/master/python/tutorial\_notebooks/emt\_tutorial.ipynb}).

\subsection{Preprocessing hgForebrainGlut and DentateGyrus data}
\label{sec:preprocessing_velocyto}
The hgForebrainGlut and DentateGyrus datasets are preprocessed by replicating the process provided by velocyto\cite{LaManno2018} here
\url{https://github.com/velocyto-team/velocyto-notebooks/blob/master/python/hgForebrainGlutamatergic.ipynb} and here \url{https://github.com/velocyto-team/velocyto-notebooks/blob/master/python/DentateGyrus.ipynb}. The package SCANPY is used to carry out the computations\cite{SCANPY}.

\subsection{Preservation of variance and PCA computation}
\label{sec:var-calc}

To estimate the variance structure of our expression matrix before and after denoising, we take the standard normalization of each variable \(j\) in the data so that each observation \(k\) in column \(j\) is
\begin{equation}
  \check{x}_{k, j} = \frac{x_{k, j} - \mathbb{E}_k \ x_{k,j}}{\sigma{(x_{,j})}}
\end{equation}
with \(\mathbb{E}_k\) denoting expected value with respect to $k$ and \(\sigma\) indicating standard deviation.

Computing the singular value decomposition, we get
\begin{equation}
  \check{\mX} = \mU \mS \mV^{T}
\end{equation}

The singular values are invariant to the transpose, meaning that they explain the variance in the data independent of whether the data is projected onto cells or genes, and nonzero singular values are bounded by \(\min{\{m,n\}}\). To estimate the rank of \(\check{\mX}\) and the nonzero singular values we use the cutoff from numpy\cite{numpy}:
\begin{equation}
  \label{nonzeros}
  \mS \le \max{(\mS)} \times \max{\{m,n\}} \times \epsilon
\end{equation}
with \(\epsilon\) being the machine precision of a numpy float 32 type. 

The relative variance is then calculated as
\begin{equation}
  \eta_{i}^2 = \frac{s_{i}^{2}}{\sum_{i} s_{i}^{2}}
\end{equation}

The relative condition number of each singular value can be calculated as

\begin{equation}
  |\kappa_{i}| = \frac{s_{i}}{\ubar{s}}
\end{equation}
with \(\ubar{s}\) representing the minimum nonzero singular value defined by equation \ref{nonzeros}.

\subsection{Processing \citet{Tian2019} benchmark data}
\label{sec:scbenchmark}

In order to evaluate \dewakss and existing methods in accordance with the benchmark analysis of \citet{Tian2019}, we use the R code provided in \citet{Tian2019_github} to apply all normalization methods that we can successfully run on the RNAmix\_CEL-seq2 and RNAmix\_Sort-seq datasets. We also apply our FTT-based preprocessing method on the data in Python and combine the result with the other normalization results into a single data structure.

We then use the same codebase to run the denoising (imputation) methods in \cite{Tian2019} on the output of each of the normalization methods on each dataset. We transfer each of these outputs to Python, perform a hyperparameter search using \dewakss on it and record the best parameter configurations along with the corresponding mean squared error (MSE) in Table \ref{tab:hp_search_results}. We apply \dewakss on each normalized input using the optimal configuration for that input and transfer the results back to R, combining them with the other denoising results into a single data structure. Note that some normalization-imputation method combinations are not represented in our figures as we could not successfully run these using the provided pipeline. We use the postprocessing and plotting scripts in \cite{Tian2019_github} to generate plots for our analysis.

\subsection{Benchmarking computational performance}
\label{sec:speedbenchmark}

To benchmark the computational cost of \dewakss we select data sets with varying number of genes and cells from the previously used datasets adding \cite{Zeisel2015} using \dewakss default configurations and similar preprocessing for all datasets.
Data sets are filtered for expressed genes; genes which are expressed in fewer than 30 cells are removed from the data set. 
Any gene which does not have a minimum of 30 counts in at least one cell is also removed.
For SAVER and DeepImpute, data is provided directly as integer counts; for all other methods, the expression data is log\textsubscript{2}(x+1) transformed.
All methods are used with default configurations. 
Some methods such as DeepImpute pre-filter their features to a fraction of the input. To test how this influences the performance we remove this filter for DeepImpute and add the filter to \dewakss.
The hardware used is Memory: 31.1GiB, Processor: Intel\textregistered Core\texttrademark i9-8950HK CPU @ 2.90GHz \(\times\) 12, Graphics: GeForce GTX 1050 Ti with Max-Q Design/PCIe/SSE2, OS type: Ubuntu 18.04 64-bit, Swap disk: 64GB.
If the method requires a parameter for the number of processors to use, it is set to 12, the maximum number of available cores.
